# README #

How to use our lovely P2P network.

### Steps to get up and running ###

* Connect to VPN
* Create application with our network
* Running the network

### Connecting to VPN ###

Why do you need to connect to a VPN at all? Well, in a perfect world, we would have no reason to block ports, so peer to peer connections would work seamlessly, but in this world ISPs and Neumont block peer to peer interactions, so you must join the VPN network in order to communicate directly with other peers.

My VPN server is hosted at 52.32.178.25 and uses PPTP


```
#!text
uname   |       password        
-----------------------------------
test1   |       bitcoinpass     
test2   |       bitcoinpass     
test3   |       bitcoinpass     
test4   |       bitcoinpass     
test5   |       bitcoinpass     
```

You may want to [connect without losing internet connectivity](http://www.nextofwindows.com/how-to-use-local-internet-connection-to-access-internet-while-still-connected-with-vpn) or [review the steps to setting up the vpn in Windows 8](http://www.ibvpn.com/billing/knowledgebase/73/Set-up-the-PPTP-VPN-on-Windows-8.html).

### Creating an application with our network ###

Setting up an application with our network is easy! All you need to do is create a new P2PNode and register your implementation of INetworkListener with it. Here's an example program:


```
#!c#

P2PNode node = new P2PNode(new []{192.168.3.235}); // Put your VPN ip here

MyListener listener = new MyListener(); // Your implementation of INetworkListener

node.RegisterListener(listener);

UserRegistration registration = new UserRegistration(){
   Username = "bob",
   PublicKey = "not implemented.................", // must be 32 characters long
   Timestamp = DateTime.Now.ToBinary ()
};

node.RegisterUser(registration);

InstantMessage msg = new InstantMessage(){
   Text = "Test message",
   UserName = "bob",
   Timestamp = DateTime.Now.ToBinary (),
   Hash = "not implemented................." // must be 32 characters long
};

node.Send(msg);

```
### Running the Network ###

1. Identify what ip address the computer running the server has on the VPN
2. Run the Server, passing in its ip address (we can't guess that in this case because there are multiple network interfaces i.e. the VPN, wireless, wired, so we must be told which ip address to bind to)
3. Pass in the ip address as a command line argument and inside your program pass that information to P2PNode in the constructor
4. Run other instances of P2PNode on other computers
5. Attempt to send messages across the network